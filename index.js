var blueDevice = require('./blueDevice');

console.log('Waiting for connect...');


blueDevice.discoverAll(function(ble_uart) {
    // enable disconnect notifications
    console.log('Device ' + ble_uart.uuid + ' Discovered! connecting..');

    ble_uart.on('disconnect', function() {
        console.log('disconnected!: ', ble_uart.uuid);
    });

    // connect and setup
    ble_uart.connectAndSetup(function() {
        // var writeCount = 0;

        console.log('connected!', ble_uart.uuid);

        // ble_uart.readDeviceName(function(devName) {
        //     console.log('Device name:', devName);
        // });

        ble_uart.on('data', function(data) {
            // console.log(ble_uart);
            var str_data = data.toString();
            var arr_data = str_data.split(';');
            console.log(new Date(), ble_uart._peripheral.id, ble_uart._peripheral.rssi, arr_data[0], arr_data[1], arr_data[2]);
        });

        // setInterval(function() {
        //     var TESTPATT = 'Hello world! ' + writeCount.toString();
        //     ble_uart.write(TESTPATT, function() {
        //         console.log('data sent:', TESTPATT);
        //         writeCount++;
        //     });
        // }, 3000);
    });
});
