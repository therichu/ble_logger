# README #

MeasureAid Blue Beacon BLE uart Monitoring Tool

### 환경 ###

Node.js 0.10 버전 사용

Windows Installer: https://nodejs.org/dist/v0.10.35/node-v0.10.35-x86.msi
Windows x64 Installer: https://nodejs.org/dist/v0.10.35/x64/node-v0.10.35-x64.msi
Windows x64 Files: https://nodejs.org/dist/v0.10.35/x64/


### 사용법 ###

```
 > npm install
 > node .
```


### 결과 화면 ###
```
 ~/Development/ble_logger_monitor   master  node .
connected!
Wed Nov 11 2015 11:18:10 GMT+0900 (KST) '3967909aa3294230a23a4b54535280ff' -68 '-1608' '-84' '16052'
Wed Nov 11 2015 11:18:11 GMT+0900 (KST) '3967909aa3294230a23a4b54535280ff' -68 '-1540' '-88' '16096'
Wed Nov 11 2015 11:18:12 GMT+0900 (KST) '3967909aa3294230a23a4b54535280ff' -68 '-1580' '-100' '16192'
Wed Nov 11 2015 11:18:13 GMT+0900 (KST) '3967909aa3294230a23a4b54535280ff' -68 '-1560' '-32' '16136'
Wed Nov 11 2015 11:18:13 GMT+0900 (KST) '3967909aa3294230a23a4b54535280ff' -68 '-1636' '-36' '16208'
```
일시 UUID RSSI 가속도x 가속도y 가속도z